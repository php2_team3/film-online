<?php
App::uses('AppController', 'Controller');
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 1/6/2017
 * Time: 7:10 PM
 */
class RequestsController extends AppController
{
    public $components=array('Paginator', 'Flash', 'RequestHandler');
    public $helpers = array('Html', 'Flash', 'Form');
    public $layout='admin';

    public function index(){
        $list = $this->Request->find('all');
        $this->set('list_film_request',$list);
    }

    public function update($id=null){
        $this->autoRender = false;
        if (!$this->Request->exists($id)){
            throw new NotFoundException(__('Invalid Request'));
        }
        if($this->request->is('post')){
            $data = $this->request->data;
            if($this->Request->save($data)){
                $return = 1;
            }
            else{
                $return = 0;
            }
            $this->set('return',$return);
        }
    }

    public function delete($id=null){
        $this->autoRender = false;
        if (!$this->Request->exists($id)){
            throw new NotFoundException(__('Invalid Request'));
        }
        if($this->Request->delete($id)){
            $return = 1;
        }
        else{
            $return = 0;
        }
        echo json_encode(array('return'=>$return));
    }
}