<?php
App::uses('AppController', 'Controller');
App::uses('User_profile', 'App/Model');
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 12/12/2016
 * Time: 7:20 PM
 */
class UsersController extends AppController
{
    public $components=array('Paginator', 'Flash','RequestHandler');

    public function beforeFilter(){
        $this->Auth->allow('add');
    }
    /**
     * log user in admin system
     */
    public function login()
    {
        $this->layout = 'login';

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    /**
     * log user out from admin system
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * display a table of users with pagination & filter
     * <pre>
     * if user.role is editor
     *  display user only
     * else
     *  display all users but not the current-logged-in user
     * </pre>
     * for all users, we can: active/ de-active selected users
     * foreach user, we can: update profile/ change password/ delete
     */
    public function index()
    {
        $this->User->recursive = 0;

        if($this->Auth->user('role')==3){
            $this->set('users',$this->paginate());
        }
        else{
            $this->set('users', $this->paginate('User', array('role'=>'1')));
        }

    }

    /**
     * add new User
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $this->User->create();
//            $filename=WWW_ROOT.DS.'documents'.DS.$this->data['users']['doc_file']['filepath'];
//            move_uploaded_file($this->data['users']['doc_file']['tmp_name'],$filename);
            if ($this->User->save($data)) {
                return $this->redirect(array('controller'=>'users','action'=>'login'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }

            // attempt to save
            if ($this->User->save($this->request->data)) {
                return $this->redirect(array('action'=>'index'));

            // form validation failed
            } else {
                // check if file has been uploaded, if so get the file path
                if (!empty($this->User->data['User']['filepath']) && is_string($this->User->data['User']['filepath'])) {
                    $this->request->data['User']['filepath'] = $this->User->data['User']['filepath'];
                }
            }
        }
    }

    /**
     * get user profile by $id (OR current-logged-in user)
     */
    public function edit($id = null)
    {
        $this->autoRender = false;
        if(!$this->User->exists($id)){
            throw new NotFoundException(__('Invalids User'));
        }
        $user=$this->User->find('first',array('conditions'=>array('id'=>$id)));
        echo json_encode($user);
    }
    /**
     * update profile by $id
     */
    public function update($id=null){
        $this->autoRender = false;
        if(!$this->User->exists($id)){
            throw new NotFoundException(__('Invalids User'));
        }

        if($this->request->is('put')){
            var_dump($this->request->data);
            if($this->User->save($this->request->data)){
                $user=$this->User->find('first',array('conditions'=>array('id'=>$id)));
                echo json_encode($user);
            }
            echo json_encode($this->request->data);
        }
    }

    /**
     * update user specified by $id (OR current-logged-in user) with new password
     */
    public function changePassword($id = null)
    {
    }


    /**
     * delete user with id specified
     */
    public function delete($id=null){
        $this->autoRender = false;
        if (!$this->User->exists($id)){
            throw new NotFoundException(__('Invalid User'));
        }
        if($this->User->delete($id)){
            $return = 1;
        }
        else{
            $return = 0;
        }
        echo json_encode(array('return'=>$return));
    }

    /**
     * Profile
     */
    public function profile(){

    }
}