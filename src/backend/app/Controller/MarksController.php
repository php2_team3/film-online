<?php

/**
 * Created by PhpStorm.
 * User: Trong
 * Date: 1/13/2017
 * Time: 8:37 PM
 */
class MarksController extends AppController
{
    public function beforeFilter() {
        $this->loadModel('User');
    }
    /**
     * display all marked Films by user specified by $userid
     * @effects <pre>
     * if $userid == null || $userid not exists
     *  redirect to users/index
     * </pre>
     *
     * @param null $userid
     */
    public function index($userid = null)
    {
        if ($userid == null || !$this->User->exists($userid)) {
            return $this->redirect(['controller'=>'users', 'action'=>'index']);
        }

        $this->Mark->recursive = 0;
        $marks = $this->Mark->find('all', [
            'conditions' => ['user_id'=>$userid]
        ]);

        // view data
        $this->set('marks', $marks);
    }

}