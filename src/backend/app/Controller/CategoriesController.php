<?php

/**
 * manage categories functions
 */
class CategoriesController extends AppController
{
    public $components=array('Paginator');

    /**
     * view all categories with pagination, filter functions
     * for each category, we can: update/ delete/ view all Films classified to this
     */
    public function index() {

    }

    /**
     * add new category
     */
    public function add() {
        $this->layout=null;
        $this->autoRender = false;
        if($this->request->is('post')){
            $data = $this->request->input('json_decode');
            $this->Category->create();
            if($this->Category->save($data)){
                echo json_encode(array('result'=>'1'));
            }
            else{
                echo json_encode(array('result'=>'0'));
            }
        }
    }

    /**
     * update category with specified $id
     */
    public function edit($id=null) {
        $this->layout=null;
        $this->autoRender = false;
        if(!$this->Category->exists($id)){
            throw new NotFoundException(__('Invalids Category'));
        }
        $category=$this->Category->find('first',array('conditions'=>array('id'=>$id)));
        echo json_encode($category);
    }

    public function update($id=null){
        $this->layout=null;
        $this->autoRender = false;
        if(!$this->Category->exists($id)){
            throw new NotFoundException(__('Invalids Category'));
        }
        if($this->request->is('post')){
            $data = $this->request->input('json_decode');
            if($this->Category->save($data)){
                $category=$this->Category->find('first',array('conditions'=>array('id'=>$id)));
                echo json_encode($category);

            }else{
                echo json_encode(array('result'=>0));;
            }
        }
    }
    /**
     * delete category with specified $id
     */
    public function delete($id) {
        $this->layout=null;
        $this->autoRender = false;
        $this->request->allowMethod('post');
        if (!$this->Category->exists($id)){
            throw new NotFoundException(__('Invalid Request'));
        }
        if($this->Category->delete($id)){
            $return = 1;
        }
        else{
            $return = 0;
        }
        echo json_encode(array('return'=>$return));
    }

    public function view($id=null){
        if(!$this->Category->exists($id)){
            throw new NotFoundException(__('Invalids Category'));
        }
        $data=$this->Category->find('all',array('conditions'=>array('Category.id'=>$id)));
        $this->set('data',$data);
    }
}