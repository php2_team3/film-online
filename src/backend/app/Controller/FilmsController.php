<?php
App::uses('AppController','Controller');
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 1/14/2017
 * Time: 9:46 PM
 */
class FilmsController  extends AppController
{
    public $components=array('Paginator');

    public function index(){
    }

    public function add(){
        $this->layout=null;
        $this->autoRender = false;
        if($this->request->is('post')){
            $data=$this->request->input('json_decode');
            $this->Film->create();
            if($this->Film->save($data)){
                echo json_encode(array('result'=>'1'));
            }
            else{
                echo json_encode(array('result'=>'0'));
            }
        }
    }

    public function edit($id=null){
        $this->layout=null;
        $this->autoRender=false;
        if(!$this->Film->exists($id)){
            throw new NotFoundException(__('Invalid Film'));
        }
        $film=$this->Film->find('first',array('conditions'=>array('Film.id'=>$id)));
        echo json_encode($film);
    }

    public function update($id=null){
        $this->layout=null;
        $this->autoRender=false;
        if(!$this->Film->exists($id)){
            throw new NotFoundException(__('Invalid Film'));
        }
        if($this->request->is('post')){
            $data = $this->request->input('json_decode');
            if($this->Film->save($data)){
                $film=$this->Film->find('first',array('conditions'=>array('Film.id'=>$id)));
                echo json_encode($film);
            }else{
                echo json_encode(array('result'=>0));
            }
        }
    }

    public function delete($id) {
        $this->layout=null;
        $this->autoRender = false;
        $this->request->allowMethod('post');
        if (!$this->Film->exists($id)){
            throw new NotFoundException(__('Invalid Request'));
        }
        if($this->Film->delete($id)){
            $return = 1;
        }
        else{
            $return = 0;
        }
        echo json_encode(array('return'=>$return));
    }
}