<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 1/16/2017
 * Time: 9:22 PM
 */

class ApiController extends AppController {

    public $components = array('RequestHandler','Paginator');

    /**
     * API SHOW ALL CATEGORY
     */
    public function categories() {
        $this->loadModel('Category');
        $total_items=count($this->Category->find('all'));
       /* $number_page = ($total_data/2)-(($total_data%2)/2)+1;*/
        $perPage = 10;
        if(!isset($this->request->params['named']['page'])){
            $current_page=1;
        }else{
            $current_page = $this->request->params['named']['page'];
        }
        $this->Category->recursive=1;
        $this->Paginator->settings=array(
            'limit'=>$perPage
        );
        $this->set(array(
            'data' => $this->paginate('Category'),
            'perPage'=>$perPage,
            'totalItems'=> $total_items,
            'currentPage'=>$current_page,
            '_serialize' => array('data','totalItems','currentPage', 'perPage')
        ));
    }

    /**
     * API SHOW ALL FILM
     */
    public function films() {
        $this->loadModel('Film');
        $total_items=count($this->Film->find('all'));
        /* $number_page = ($total_data/2)-(($total_data%2)/2)+1;*/
        $perPage = 5;
        if(!isset($this->request->params['named']['page'])){
            $current_page=1;
        }else{
            $current_page = $this->request->params['named']['page'];
        }
        $this->Film->recursive=0;
        $this->Paginator->settings=array(
            'limit'=>$perPage
        );
        $this->set(array(
            'data' => $this->paginate('Film'),
            'categories'=>$this->Film->Category->find('all'),
            'perPage'=>$perPage,
            'totalItems'=> $total_items,
            'currentPage'=>$current_page,
            '_serialize' => array('data','totalItems','currentPage', 'perPage', 'categories')
        ));
    }

    /**
     *  API SHOW ALL USER
     */
    public function users() {
        $this->loadModel('User');
        $total_items=count($this->User->find('all'));
        /* $number_page = ($total_data/2)-(($total_data%2)/2)+1;*/
        $perPage = 5;
        if(!isset($this->request->params['named']['page'])){
            $current_page=1;
        }else{
            $current_page = $this->request->params['named']['page'];
        }
        $this->User->recursive=0;
        $this->Paginator->settings=array(
            'limit'=>$perPage
        );
        if($this->Auth->user('role')==3){
            $data=$this->paginate('User');
        }
        else{
            $data= $this->paginate('User', array('role'=>'1'));
        }
        $this->set(array(
            'data' => $data,
            'perPage'=>$perPage,
            'totalItems'=> $total_items,
            'currentPage'=>$current_page,
            '_serialize' => array('data','totalItems','currentPage', 'perPage')
        ));
    }

    /**
     * API SHOW ALL REQUEST
     */
    public function requests() {
        $this->loadModel('Request');
        $total_items=count($this->Request->find('all'));
        /* $number_page = ($total_data/2)-(($total_data%2)/2)+1;*/
        $perPage = 10;
        if(!isset($this->request->params['named']['page'])){
            $current_page=1;
        }else{
            $current_page = $this->request->params['named']['page'];
        }
        $this->Request->recursive=0;
        $this->Paginator->settings=array(
            'limit'=>$perPage
        );
        $this->set(array(
            'data' => $this->paginate('Request'),
            'perPage'=>$perPage,
            'totalItems'=> $total_items,
            'currentPage'=>$current_page,
            '_serialize' => array('data','totalItems','currentPage', 'perPage')
        ));
    }

    /**
     * @param $id
     * DELETE CATEGORY
     */
    public function delete($id) {
        if ($this->Category->delete($id)) {
            $message = 'Deleted';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }
}