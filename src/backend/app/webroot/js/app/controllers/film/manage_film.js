/**
 * Created by THINHDO on 1/9/2017.
 */
var app = angular.module('film', ['ui.bootstrap']);
app.controller('manage_film', ['dataFactory', '$scope', function (dataFactory, $scope, $http) {
    $('#menu_films').addClass('active');
    $('#menu_films').css({'margin': '20px 0'});

    var k=0;
    $scope.data=[];
    $scope.perPage = 0;
    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.categories_data=[];
    $scope.pageChanged = function (newPage) {
        getData(newPage);
    };
    getData(1);
    function getData(page) {
        dataFactory.httpRequest('api/films/page:'+page+'.json').then(function (data) {
            $scope.data = data.data;
            $scope.categories_data = data.categories;
            $scope.perPage = data.perPage;
            $scope.totalItems = data.totalItems;
            $scope.currentPage = page;
            console.log($scope.categories_data);

            if(k!=1){
                //get categories
                if($scope.categories.length<=$scope.categories_data.length){
                    for (var i = 0; i < $scope.categories_data.length; i++) {
                        $scope.categories.push($scope.categories_data[i]);
                    }
                }

                //get list_parent
                for (var i = 0; i < $scope.data.length; i++) {
                    if ($scope.data[i].Film.film_parent == 0) {
                        $scope.list_parent.push($scope.data[i]);
                    }
                }

                k=1;
            }



        });

    }
    //filter films by category
    /*var categories = JSON.parse(categories_json);
    $scope.categories_data = categories;*/
    var firstItem = new Object();
    firstItem.Category = {
        id: 'null',
        cat_name: 'Category'
    };
    $scope.categories = [];
    $scope.categories.push(firstItem);
    for (var i = 0; i < $scope.categories_data.length; i++) {
        $scope.categories.push($scope.categories_data[i]);
    }
    $scope.filterCategory = $scope.categories[0];
    console.log($scope.categories);

    //filter
    $scope.filter_category = function (data) {
        if (data.Film.cat_id === $scope.filterCategory.Category.id) {
            return true;
        } else if ($scope.filterCategory.Category.id === 'null') {
            return true;
        } else {
            return false;
        }

    }

    //get list_parent
    var firstParent = new Object();
    firstParent.Film = {
        id: 0,
        film_title: 'Film parent'
    }
    $scope.list_parent = [];
    $scope.list_parent.push(firstParent);

    //get statuses
    $scope.statuses = [
        {'value': 'null', 'status': 'status'},
        {'value': 'publish', 'status': 'Publish'},
        {'value': 'unpublish', 'status': 'Unpublish'},
        {'value': 'draft', 'status': 'Draft'},
        {'value': 'trailer', 'status': 'Trailer'}

    ];


    //add film
    $scope.category_selected = $scope.categories[0];
    $scope.parent_selected = $scope.list_parent[0];
    $scope.status_selected = $scope.statuses[0];
    $scope.addFilm = function () {
        var film_date = new Date();
        var data_request = new Object();
        data_request.Film = {
            film_author: $scope.author,
            film_date : film_date,
            film_thumb: $scope.form.add.film_thumb,
            film_title: $scope.form.add.film_title,
            link_film: $scope.form.add.link_film,
            link_trailer: $scope.form.add.link_trailer,
            country: $scope.form.add.country,
            cat_id: $scope.category_selected.Category.id,
            times_view: $scope.form.add.time_view,
            publishing_year: $scope.form.add.publishing_year,
            director: $scope.form.add.director,
            actor: $scope.form.add.actor,
            film_parent: $scope.parent_selected.Film.id,
            film_status : $scope.status_selected.value,
            film_content: $scope.form.add.film_content
        };
        dataFactory.httpRequest('films/add', 'POST', {}, data_request).then(function (data) {
            console.log($scope.form.add.film_thumb);
            if (data.result == '1') {
                $(".modal").modal("hide");
                data_request.Category={
                    cat_name:$scope.category_selected.Category.cat_name
                };
                getData($scope.currentPage);
            }
        });
        console.log($scope.data);
    }

    //get infor to edit
    $scope.edit=function (id) {
        dataFactory.httpRequest('films/edit/'+id).then(function (data) {
            for(var i=0;i<$scope.categories.length;i++){
                if($scope.categories[i].Category.cat_name==data.Category.cat_name){
                    $scope.category_selected_edit = $scope.categories[i];
                }
            }

            for(var i=0;i<$scope.statuses.length;i++){
                if($scope.statuses[i].value==data.Film.film_status){
                    $scope.status_selected_edit = $scope.statuses[i];
                }
            }

            for(var i=0;i<$scope.list_parent.length;i++){
                if($scope.list_parent[i].Film.id==data.Film.film_parent){
                    $scope.parent_selected_edit = $scope.list_parent[i];
                }
            }

            $scope.form=data;
            console.log(data);
        })
    };

    $scope.saveEdit = function () {
        var modified=new Date()
        var data_request = new Object();
        data_request.Film = {
            id:$scope.form.Film.id,
            film_date:$scope.form.Film.film_date,
            film_modified:modified,
            film_modified_user_id:$scope.modifier,
            film_author: $scope.form.Film.film_author,
            film_thumb: $scope.form.Film.film_thumb,
            film_title: $scope.form.Film.film_title,
            link_film: $scope.form.Film.link_film,
            link_trailer: $scope.form.Film.link_trailer,
            country: $scope.form.Film.country,
            cat_id: $scope.category_selected_edit.Category.id,
            times_view: $scope.form.Film.time_view,
            publishing_year: $scope.form.Film.publishing_year,
            director: $scope.form.Film.director,
            actor: $scope.form.Film.actor,
            film_parent: $scope.parent_selected_edit.Film.id,
            film_status : $scope.status_selected_edit.value,
            film_content: $scope.form.Film.film_content
        };

        dataFactory.httpRequest('films/update/'+$scope.form.Film.id, 'POST', {},data_request).then(function (data) {
            $(".modal").modal("hide");
            data_request.Category={
                cat_name:$scope.category_selected_edit.Category.cat_name
            };
            $scope.data = apiModifyFilm($scope.data,$scope.form.Film.id,data_request);
            console.log($scope.data);
        })
    };

    $scope.delete = function (id, index) {
        var result = confirm('Bạn có muốn xóa film này?');
        if(result){
            dataFactory.httpRequest('films/delete/'+id,'POST').then(function (data) {
               console.log(data);
                if(data.return=='1'){
                    getData($scope.currentPage);
                }
                console.log(data);
            })
        }

    };

}]);