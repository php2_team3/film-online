/**
 * Created by THINHDO on 1/6/2017.
 */
var app = angular.module('film',['ui.bootstrap']);
app.controller('film_request',['dataFactory','$scope', function(dataFactory,$scope,$http){
    $('#requests').addClass('active');
    $('#requests').css({'margin': '20px 0'});

    $scope.perPage = 0;
    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageChanged = function (newPage) {
        getData(newPage);
    };
    getData(1);
    function getData(page) {
        dataFactory.httpRequest('api/requests/page:'+page+'.json').then(function (data) {
            $scope.data = data.data;
            $scope.perPage = data.perPage;
            $scope.totalItems = data.totalItems;
            $scope.currentPage = page;
            console.log($scope.data[1] );
        });

    }

    $scope.change_status = function (id,index) {

        $scope.data[index].Request.status=true;
        dataFactory.httpRequest('requests/update/'+id,'POST',{}, $scope.data[index]).then(function (data) {
            if(data==1){
                $scope.data[index].Request.status=true;
            }
            console.log($scope.data[index].Request);
        })
    };

    $scope.delete = function (id, index) {
        var result = confirm('Bạn có muốn xóa yêu cầu này?');
        if(result){
            dataFactory.httpRequest('requests/delete/'+id,'POST').then(function (data) {
                alert(data);
                if(data=='1'){

                    $scope.data.splice(index,1);
                }
                console.log(data);
            })
        }

    };
}]);