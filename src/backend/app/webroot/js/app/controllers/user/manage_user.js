/**
 * Created by THINHDO on 1/10/2017.
 */
var app= angular.module('user',['ui.bootstrap']);
app.controller('manage_user',['dataFactory','$scope',function (dataFactory,$scope,$http) {
    $('#menuUsers').addClass('active');
    $('#menuUsers').css({'margin': '20px 0'});

    $scope.perPage = 0;
    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageChanged = function (newPage) {
        getData(newPage);
    };
    getData(1);
    function getData(page) {
        dataFactory.httpRequest('api/users/page:'+page+'.json').then(function (data) {
            $scope.data = data.data;
            $scope.perPage = data.perPage;
            $scope.totalItems = data.totalItems;
            $scope.currentPage = page;
            console.log($scope.data[1] );
        });

    }

    //filter by status
    $scope.statuses= [
        {'value':'null','status':'status'},
        {'value':true,'status':'active'},
        {'value':false, 'status':'closed'}
    ];

    $scope.filterItem = $scope.statuses[0];
    //filter status
    $scope.filter_status=function(data){
        console.log($scope.filterItem.value);
        if(data.User.status===$scope.filterItem.value){
            return true;
        }else if($scope.filterItem.value==='null'){
            return true;
        }else {
            return false;
        }

    }

    //get user profile
    $scope.edit = function(id){
        dataFactory.httpRequest('users/edit/'+id).then(function (data) {
            $scope.form = data;
            var src = $('#avatar_img').attr('src');
            $('#avatar_img').attr({'src':src+$scope.form.User.avatar});
            var dob=new Date($scope.form.User.dob);
            $scope.form.User.dob={
                day:dob.getDate().toString(),
                month:(dob.getMonth()+1).toString(),
                year:dob.getFullYear().toString()
            };
            if((dob.getDate()/10)<1){
                $scope.form.User.dob.day='0'+$scope.form.User.dob.day;
            }
            if(((dob.getMonth()+1)/10)<1){
                $scope.form.User.dob.month='0'+$scope.form.User.dob.month;
            }

            console.log( dob.getMonth());
        });
    }
    
    //update user profile
    $scope.saveEdit=function () {
        console.log($scope.form);
        dataFactory.httpRequest('users/update/'+$scope.form.User.id,'PUT',{},$scope.form).then(function (data) {
            $(".modal").modal("hide");
            $scope.data = apiModifyUser($scope.data,data.User.id,data);
            console.log($scope.data);
        });
    }

    //delete user
    $scope.delete = function (id, index) {
        var result = confirm('Bạn có muốn xóa thành viên này?');
        if(result){
            dataFactory.httpRequest('users/delete/'+id,'POST').then(function (data) {
                alert(data);
                if(data=='1'){

                    $scope.data.splice(index,1);
                }
                console.log(data);
            })
        }

    };
}]);