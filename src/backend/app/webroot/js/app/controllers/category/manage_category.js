/**
 * Created by THINHDO on 1/13/2017.
 */
var app=angular.module('category',['ui.bootstrap']);
app.controller('manage_category', ['$scope', 'dataFactory', function ($scope,dataFactory, $http) {
    $('#categories').addClass('active');
    $('#categories').css({'margin': '20px 0'});
    //$scope.data=JSON.parse(categories_json);
    $scope.perPage = 0;
    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageChanged = function (newPage) {
        getData(newPage);
    };
    getData(1);
    function getData(page) {
        dataFactory.httpRequest('api/categories/page:'+page+'.json').then(function (data) {
            $scope.data = data.data;
            $scope.perPage = data.perPage;
            $scope.totalItems = data.totalItems;
            $scope.currentPage = page;
            console.log($scope.data[1] );
        });

    }

    //add category
    $scope.saveAdd= function () {
        dataFactory.httpRequest('categories/add','POST',{},$scope.form.add).then(function (data) {
            console.log(data);
            if(data.result=="1"){
                $(".modal").modal("hide");
                getData($scope.currentPage);
            }
        })
    }
    //get information to edit
    $scope.edit = function (id) {
        dataFactory.httpRequest('categories/edit/'+id).then(function (data) {
            console.log(data);
            $scope.form=data;
            console.log($scope.form);
        })
    }

    //update category
    $scope.saveEdit = function () {
        dataFactory.httpRequest('categories/update/'+$scope.form.Category.id,'POST',{}, $scope.form).then(function (data) {
            $(".modal").modal("hide");
            $scope.data = apiModifyCategory($scope.data,data.Category.id,data);
        })
    }
    
    //delete category
    $scope.delete = function (id,index) {
        var result = confirm('Bạn có muốn xóa category này?');
        dataFactory.httpRequest('categories/delete/'+id, 'POST').then(function (data) {
          getData($scope.currentPage);
        })
    }
}]);