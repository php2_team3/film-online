/**
 * Created by THINHDO on 1/16/2017.
 */
var app = angular.module('category', []);
app.controller('view', ['dataFactory', '$scope', function (dataFactory, $scope, $http) {
    $('#categories').addClass('active');
    $('#categories').css({'margin': '20px 0'});

    var data = JSON.parse(data_json);
    $scope.Category = data[0].Category;
    $scope.data = data[0].Film;
    console.log($scope.data);

    //get statuses
    $scope.statuses = [
        {value: 'null', status: 'status'},
        {value: 'publish', status: 'Publish'},
        {value: 'unpublish', status: 'Unpublish'},
        {value: 'draft', status: 'Draft'},
        {value: 'trailer', status: 'Trailer'}

    ];
    $scope.filterStatus=$scope.statuses[0];
    $scope.filter_status = function (data) {
        if (data.film_status === $scope.filterStatus.value) {
            return true;
        } else if ($scope.filterStatus.value === 'null') {
            return true;
        } else {
            return false;
        }

    }
}]);
