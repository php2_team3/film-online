function apiModifyCategory(originalData,id,response){
    angular.forEach(originalData, function (item,key) {
        if(item.Category.id == id){
            originalData[key] = response;
        }
    });
    return originalData;
}


function apiModifyUser(originalData,id,response){
    angular.forEach(originalData, function (item,key) {
        if(item.User.id == id){
            originalData[key] = response;
        }
    });
    return originalData;
}

function apiModifyFilm(originalData,id,response){
    angular.forEach(originalData, function (item,key) {
        if(item.Film.id == id){
            originalData[key] = response;
        }
    });
    return originalData;
}

