<?php
App::uses('AppModel', 'Model');
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 1/13/2017
 * Time: 9:35 AM
 */
class Category extends AppModel
{
    public $hasMany=array(
        'Film'=>array(
            'className'=>'Film',
            'foreignKey'=>'cat_id'
        )
    );
}