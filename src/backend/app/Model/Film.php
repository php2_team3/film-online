<?php
App::uses('AppModel', 'Model');
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 12/12/2016
 * Time: 8:38 PM
 */
class Film extends AppModel
{
    public $belongsTo = array(
        'Category'=>array(
            'className'=>'Category',
            'foreignKey'=>'cat_id'
        )
    );
}