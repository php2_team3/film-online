<div class="main-content">
    <div class="col-sm-12 col-md-12 col-xs-12">

        <div class="widget">
            <h3 class="section-title first-title">User profile
                <div class="pull-right">
                    <a href="add_member.html" tyle="text-decoration: none"><i class="icon-plus"></i>Edit
                        profile</a>
                </div>
            </h3>
        </div>
        <div class="widget-content-white glossed">
            <div class="row">
                <div class="col-md-2 col-sm-12 col-xs-12">
                    <div>
                        <img class="img-avatar" alt="" src="../docs/img/lightbox/DayHot1.jpg">
                    </div>
                </div>
                <div class="col-md-10 col-xs-12 col-sm-12">
                    <p class="form-control">
                        <small class="text-muted">Username:</small>
                        JSmith
                    </p>
                    <p class="form-control">
                        <small class="text-muted">Name:</small>
                        Jonathan Smith
                    </p>
                    <p class="form-control">
                        <small class="text-muted">Gender:</small>
                        Male
                    </p>
                    <p class="form-control">
                        <small class="text-muted">Birthday:</small>
                        24/06/1974
                    </p>
                    <p class="form-control">
                        <small class="text-muted">Email:</small>
                        john@example.com
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>