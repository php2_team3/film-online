<div ng-app="user">
    <div class="widget">
        <h3 class="section-title first-title"><i class="icon-table"></i> Users Table

        </h3>

        <div class="widget-content-white glossed" ng-controller="manage_user">
            <div class="padded">
                <?php
                $user = AuthComponent::user();
                if ($user['role'] == 3) {
                    ?>
                    <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'add')); ?>"
                       class="pull-right btn btn-default" style="text-decoration: none; margin: 20px 0 "><i
                            class="icon-plus"></i>Add User</a>
                <?php } ?>
                <table class="table table-striped table-bordered table-hover datatable">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td><input type="text" class="form-control" placeholder="Search" ng-model="searchText"
                                   name="table_search"></td>
                        <td></td>
                        <td><select class="form-control" ng-model="filterItem"
                                    ng-options="item.status for item in statuses"></select></td>
                        <td>
                        </td>
                    </tr>
                    <tr ng-repeat="value in data | filter :filter_status| filter:searchText">
                        <td>{{$index+1}}</td>
                        <td>{{value.User.username}}</td>
                        <td>{{value.User.email}}</td>
                        <td>
                            <span class="label label-success" ng-show="value.User.status==true">active</span>
                            <span class="label label-danger" ng-show="value.User.status==false">closed</span>
                        </td>
                        <td class="text-center">
                            <a href="films/viewUpload/{{value.User.id}}" ng-show="value.User.role==2"
                               class="btn btn-default btn-xs"><i
                                    class="icon-upload-alt"></i></a>
                            <a href="marks/index/{{value.User.id}}" ng-show="value.User.role==1"
                               class="btn btn-default btn-xs"><i
                                    class="icon-bookmark"></i></a>
                            <?php
                            $user = AuthComponent::user();
                            if ($user['role'] == 3) {
                                ?>
                                <a href="#" class="btn btn-default btn-xs" ng-click="edit(value.User.id,$index)"
                                   data-toggle="modal" data-target="#edit_user"><i class="icon-pencil"></i></a>
                            <?php } ?>
                            <a href="#" class="btn btn-danger btn-xs" ng-click=delete(value.User.id,$index)><i
                                    class="icon-remove"></i></a>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <!-- Modal -->
                <div class="modal fade" id="edit_user" role="dialog">
                    <div class="modal-dialog" style="width: 800px;">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Edit User</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <form method="POST" name="editItem" role="form" ng-submit="saveEdit()">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <!--<div class="form-group text-center">
                                                    <?php
                                                /*                                                    echo $this->Html->image("", array('class' => 'img-avatar', 'id' => 'avatar_img'));
                                                                                                    echo $this->Form->file('avatar', array('onchange' => 'readURL(this);'));
                                                                                                    */ ?>
                                                </div-->>

                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label for="username">User name</label>
                                                    <input type="text" class="form-control username" name="username"
                                                           placeholder="User name" id="username"
                                                           ng-model="form.User.username">
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label for="full_name">Full Name</label>
                                                    <input type="text" class="form-control name" placeholder="Name"
                                                           name="name" id="full_name" ng-model="form.User.full_name">
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                <div class="form-group" style="margin-bottom: 0px;">
                                                    <?php
                                                    echo $this->Form->label('gender');
                                                    ?>
                                                    <div class="form-group">
                                                        <?php
                                                        $options = array('1' => 'Male', '0' => 'Female');
                                                        $attributes = array('legend' => false, 'style' => 'margin:10px', 'ng-model' => 'form.User.gender');
                                                        echo $this->Form->radio('gender', $options, $attributes);
                                                        ?>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <?php
                                                    echo $this->Form->label('dob', 'Birthday');
                                                    ?>
                                                    <div class="form-group">
                                                        <?php
                                                        echo $this->Form->day('dob', array('ng-model' => 'form.User.dob.day', 'style' => 'border-radius:4px; margin:10px', 'empty' => false)) . '-';
                                                        echo $this->Form->month('dob', array('ng-model' => 'form.User.dob.month', 'style' => 'border-radius:4px; margin:10px', 'empty' => false)) . '-';
                                                        echo $this->Form->year('dob', 1975, date('Y'), array('ng-model' => 'form.User.dob.year', 'style' => 'border-radius:4px; margin:10px', 'empty' => false));
                                                        ?>
                                                    </div>
                                                    <!--<label>Birthday</label>
                                                    <input type="date" class="form-control" name="birthday"/>-->
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="email" class="form-control" placeholder="Email"
                                                           name="email" id="email" ng-model="form.User.email">
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-xs-12 col-sm-12">
                                                <?php echo $this->Form->label('role'); ?>
                                                <div class="form-group">

                                                    <?php
                                                    $options = array(
                                                        array('name' => 'User', 'value' => '1'),
                                                        array('name' => 'Editor', 'value' => '2'),
                                                        array('name' => 'Admin', 'value' => '3'),
                                                    );
                                                    echo $this->Form->select('role', $options, array('class' => 'form-control', 'empty' => false, 'ng-model' => 'form.User.role'));
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xs-12 col-sm-12">
                                                <div class="form-group" style="margin-bottom: 0px;">
                                                    <?php
                                                    echo $this->Form->label('status');
                                                    ?>
                                                    <div class="form-group">
                                                        <?php
                                                        $options = array('1' => 'active', '0' => 'closed');
                                                        $attributes = array('legend' => false, 'style' => 'margin:10px', 'ng-model' => 'form.User.status');
                                                        echo $this->Form->radio('status', $options, $attributes);
                                                        ?>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label for="address">Adress</label>
                                                    <input type="text" id="address" class="form-control name"
                                                           placeholder="Name"
                                                           name="name" ng-model="form.User.address">
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label for="phone">Phone</label>
                                                    <input type="text" id="phone" class="form-control name"
                                                           placeholder="Name"
                                                           name="name" ng-model="form.User.phone">
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" name="submit">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <pagination style="background-color: #F5F5F5" total-items="totalItems" ng-model="currentPage"
                        ng-change="pageChanged(currentPage)" class="pagination-sm pull-right"
                        items-per-page="perPage"></pagination>

        </div>
    </div>
</>


<?php echo $this->start('javascript'); ?>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar_img')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<?php
echo $this->Html->script('app/controllers/user/manage_user');
echo $this->Html->script('app/services/myServices');
echo $this->end('javascript');
?>