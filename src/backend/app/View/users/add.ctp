<div class="main-content">
    <div class="widget-content-blue-inner glossed">
        <h3 class="first-title section-title"> Add Member</h3>
        <div class="row ">
            <?php echo $this->Flash->render(); ?>

            <?php echo $this->Form->create('User'); ?>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-group text-center">
                        <?php
                        echo $this->Html->image('default-avatar.jpg', array('class' => 'img-avatar', 'id' => 'avatar_img'));
                        echo $this->Form->file('avatar', array('onchange' => 'readURL(this);'));
                        ?>
                    </div>

                </div>
            </div>
            <div class="col-md-8">
                <!--user name-->
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="form-group">
                        <?php
                        echo $this->Form->input('username', array('class' => 'form-control', 'placeholder' => 'User name'));
                        ?>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="form-group">
                        <?php
                        echo $this->Form->input('full_name', array('class' => 'form-control', 'placeholder' => 'Full name'));
                        ?>
                    </div>
                </div>
                <!--password-->
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="form-group">
                        <?php
                        echo $this->Form->input('password', array('class' => 'form-control', 'placeholder' => 'Password'));
                        ?>
                    </div>
                </div>
                <!--gender-->
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="form-group" style="margin-bottom: 0px;">
                        <?php
                        echo $this->Form->label('gender');
                        ?>
                        <div class="form-group">
                            <?php
                            $options = array('1' => 'Male', '0' => 'Female');
                            $attributes = array('legend' => false, 'style' => 'margin:10px');
                            echo $this->Form->radio('gender', $options, $attributes);
                            ?>
                        </div>

                    </div>
                </div>
                <!--dob-->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <?php
                        echo $this->Form->label('dob', 'Birthday');
                        ?>
                        <div class="form-group">
                            <?php
                            echo $this->Form->dateTime('dob', 'DMY', null, array('style' => 'border-radius:4px; margin:10px', 'empty' => false,'minYear'=>'1975'));
                            ?>
                        </div>
                        <!--<label>Birthday</label>
                        <input type="date" class="form-control" name="birthday"/>-->
                    </div>
                </div>
                <!--email-->
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="form-group">
                        <?php
                        echo $this->Form->input('email', array('type' => 'email', 'class' => 'form-control', 'placeholder' => 'Email'));
                        ?>
                    </div>
                </div>
                <!--address-->
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="form-group">
                        <?php
                        echo $this->Form->input('address', array('class' => 'form-control', 'placeholder' => 'Address'));
                        ?>
                    </div>
                </div>
                <!--phone number-->
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="form-group">
                        <?php
                        echo $this->Form->input('phone', array('class' => 'form-control', 'placeholder' => 'Phone'));
                        ?>
                    </div>
                </div>
                <!--role-->
                <div class="form-group col-md-12 col-xs-12 col-sm-12" style="margin-bottom: 0px;">
                    <?php echo $this->Form->label('role'); ?>
                </div>
                <div class="col-md-3 col-xs-3 col-sm-3">
                    <div class="form-group">

                        <?php
                        $options = array(
                            array('name' => 'User', 'value' => '1'),
                            array('name' => 'Editor', 'value' => '2'),
                            array('name' => 'Admin', 'value' => '3'),
                        );
                        echo $this->Form->select('role', $options, array('class' => 'form-control', 'empty' => false));
                        ?>
                    </div>
                </div>

                <div class="clearfix"></div>
                <!--submit button-->
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="form-group">
                        <!-- <button class="btn btn-primary " name="submit">Add User</button>-->
                        <?php echo $this->Form->submit('add', array('class' => 'btn btn-primary'));?>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>

        </div>
    </div>
</div>

<?php echo $this->start('javascript');?>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar_img')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<?php echo $this->end('javascript')?>
