<div class="all-wrapper no-menu-wrapper">
    <div class="login-logo-w">
        <a href="<?php echo $this->Html->url('/home'); ?>" class="logo">
            <i class="icon-cloud-download"></i>
            <span>Mars Admin</span>
        </a>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <div class="content-wrapper wood-wrapper bold-shadow">
                <div class="content-inner">
                    <div class="main-content main-content-grey-gradient no-page-header">
                        <div class="main-content-inner">
                            <?php echo $this->Form->create('User'); ?>
                                <h3 class="form-title form-title-first"><i class="icon-lock"></i> Login Administrator
                                </h3>
                                <div class="form-group">
                                    <?php echo $this->Form->input('username', ['class' => 'form-control', 'placeholder' => 'Username']); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $this->Form->input('password', ['class' => 'form-control', 'placeholder' => 'Password']); ?>
                                </div>
                                <!--
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Remember me
                                        </label>
                                    </div>
                                </div>
                                -->
                                <input type="submit" class="btn btn-primary btn-lg" value="Sign in"/>
                                <input type="reset" class="btn btn-link" value="Cancel"/>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


