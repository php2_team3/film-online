<div ng-app="film">
    <div class="widget">
        <h3 class="section-title first-title"><i class="icon-table"></i>List film requested</h3>
        <div class="widget-content-white glossed">
            <div class="padded" ng-controller="film_request">
                <table class="table table-striped table-bordered table-hover datatable">
                    <thead>
                    <tr>
                        <th>
                            Mark as seen
                        </th>
                        <th>STT</th>
                        <th>Name</th>
                        <th>Times</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="value in data">
                        <td>
                            <div class="checkbox" ng-show="value.Request.status==false">
                                <input type="checkbox" ng-change="change_status({{value.Request.id}}, {{$index}})"
                                       ng-model="myValue">
                            </div>
                        </td>
                        <td>{{ $index+1 }}</td>
                        <td>{{ value.Request.name }}</td>
                        <td>{{ value.Request.times_request }}</td>
                        <td>
                            <span id="status_{{$index}}" ng-show="value.Request.status==true"
                                  class="label label-success ">seen</span>
                        </td>
                        <td class="text-center">

                            <button ng-click =delete(value.Request.id,$index)
                                class="btn btn-danger btn-xs">
                                <i class="icon-remove"></i>
                            </button>

                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <pagination style="background-color: #F5F5F5" total-items="totalItems" ng-model="currentPage" ng-change="pageChanged(currentPage)" class="pagination-sm pull-right" items-per-page="perPage"></pagination>

        </div>
    </div>
</div>

<?php echo $this->start('javascript'); ?>

<?php
echo $this->Html->script('app/controllers/film/index');
echo $this->Html->script('app/services/myServices');
echo $this->end('javascript');
?>

