<div class="page-header page-header-green">
    <div class="header-links hidden-xs">

        <a href="<?php echo $this->Html->url('/Users/profile'); ?>"><i class="icon-info"></i> Profile</a>
        <a href="<?php echo $this->Html->url('/Users/logout'); ?>"><i class="icon-signout"></i> Logout</a>
    </div>
    <h1><i class="icon-play-circle"></i> Movie Online</h1>
</div>