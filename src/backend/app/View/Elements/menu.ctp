<div class="col-md-3">
    <div class="text-center">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="side-bar-wrapper collapse navbar-collapse navbar-ex1-collapse">
        <a href="<?php echo $this->Html->url('/requests'); ?>" class="logo hidden-sm hidden-xs">
            <i class="icon-cloud-download"></i>
            <span>Mars Admin</span>
        </a>

        <div class="relative-w">
            <ul class="side-menu">
                <li id="requests">
                    <a href="<?php echo $this->Html->url('/requests'); ?>" class="active">
                        <span class="badge pull-right"></span>
                        <i class="icon-home"></i> Requested films
                    </a>
                </li>
                <li id="menuUsers">
                    <a href="<?php echo $this->Html->url('/users'); ?>" class="active">
                        <span class="badge pull-right"></span>
                        <i class="icon-user"></i> Manage Users
                    </a>
                </li>
                <li id="menu_films">
                    <a href="<?php echo $this->Html->url('/Films'); ?>">
                        <span class="badge pull-right"></span>
                        <i class="icon-picture"></i> Manages Films
                    </a>
                </li>
                <li id="categories">
                    <a href="<?php echo $this->Html->url('/categories'); ?>">
                        <span class="badge pull-right"></span>
                        <i class="icon-th"></i> Manage Categories
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>