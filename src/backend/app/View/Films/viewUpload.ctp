<div class="main-content" ng-app="film">
    <div class="widget" ng-controller="manage_film">
        <h3 class="section-title first-title"><i class="icon-table"></i>Film Table
            <div class="pull-right">
                <a href="#" data-toggle="modal" data-target="#add_film" class="btn pull-right" style="text-decoration: none" ><i class="icon-plus"></i>Add Film</a>
            </div>
        </h3>
        <div class="widget-content-white glossed">
            <div class="padded">
                <div class="form-group pull-right">
                    <select name="" id="filterCategory" class="form-control-static">
                        <option value="">category</option>
                        <option value="">aaa</option>
                        <option value="">bbbb</option>
                        <option value="">cccc</option>
                    </select>
                    <select name="" id="filterActor" class="form-control-static">
                        <option value="">actor</option>
                        <option value="">aaa</option>
                        <option value="">bbbb</option>
                        <option value="">cccc</option>
                    </select>
                    <input type="text" id="search" class="form-control-static" placeholder="search">
                </div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th><div class="checkbox"><input type="checkbox"></div></th>
                        <th>STT</th>
                        <th>Name</th>
                        <th>Actor</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><div class="checkbox"><input type="checkbox"></div></td>
                        <td>1</td>
                        <td><a href="view_film.html" style="text-decoration: none">aaaa</a></td>
                        <td>cccccc</td>
                        <td>ddddddd</td>
                        <td><span class="label label-success">Active</span></td>
                        <td class="text-center">
                            <a href="view_film.html" class="btn btn-default btn-xs"><i class="icon-eye-open"></i>detail</a>
                            <a href="edit_film.html" class="btn btn-default btn-xs"><i class="icon-pencil"></i> edit</a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="icon-remove"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td><div class="checkbox"><input type="checkbox"></div></td>
                        <td>2</td>
                        <td><a href="view_film.html" style="text-decoration: none">aaaa</a></td>
                        <td>cccccc</td>
                        <td>ddddddd</td>
                        <td><span class="label label-danger">Closed</span></td>
                        <td class="text-center">
                            <a href="view_film.html" class="btn btn-default btn-xs"><i class="icon-eye-open"></i>detail</a>
                            <a href="edit_film.html" class="btn btn-default btn-xs"><i class="icon-pencil"></i> edit</a>
                            <a href="#" class="btn btn-danger btn-xs"><i class="icon-remove"></i></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Modal create -->
        <div id="add_film" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 800px">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Add Film</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <form  name="form_create" role="form" method="POST" ng-submit="addFilm()">
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <label for="thumbnail">Thumbnails</label>
                                        <div class="form-group text-center">
                                            <?php echo $this->Html->image('default_img/film_thumb_default.png', array('class'=>'img-responsive'))?>
                                            <input type="file" id="thumbnail" class="form-control" ng-model='form.add.film_thumb' >
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="name">Film Title</label>
                                            <input type="text" id="name" ng-model="form.add.film_title" class="form-control" name="name"
                                                   placeholder="Film title">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="link_film">Link Film</label>
                                            <input type="url" id="link_film" class="form-control" placeholder="Link Film"
                                                   name="link_film" ng-model="form.add.link_film">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="link_trailer">Link Trailer</label>
                                            <input type="text" class="form-control" placeholder="Link Trailer"
                                                   name="link_trailer" ng-model="form.add.link_trailer">
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="director">Director</label>
                                            <input type="text" class="form-control" name="director" placeholder="Director"
                                                   ng-model="form.add.director"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="actor">Actor</label>
                                            <input type="text" class="form-control" placeholder="Diễn viên"
                                                   name="actor" id="actor" ng-model="form.add.actor">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <input type="text" class="form-control" placeholder="Category"
                                                   name="category" id="category" ng-model="form.add.category">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <input type="text" class="form-control" placeholder="Country"
                                                   name="country" id="country" ng-model="form.add.country">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="time_view">Time</label>
                                            <input type="text" class="form-control" placeholder="Time"
                                                   name="time" id="time_view" ng-model="form.add.time_view">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="publishing_year">Publishing year</label>
                                            <input type="text" class="form-control" placeholder="Publishing year"
                                                   name="publishing_year" ng-model="form.add.publishing_year">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label for="film_content">Nội dung phim</label>
                                        <textarea name="film_content" class="form-control" id="film_content" cols="90" rows="10"></textarea>

                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit" name="submit">Save</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php echo $this->start('javascript');?>
<script type="text/javascript">
    var films_json='<?php echo json_encode($films)?>';
</script>
<?php
echo $this->Html->script('app/controllers/film/manage_film');
echo $this->Html->script('app/services/myServices');
echo $this->end('javascript');
?>