<div class="main-content" ng-app="film">
    <div class="widget" ng-controller="manage_film">
        <h3 class="section-title first-title"><i class="icon-table"></i>Film Table

        </h3>
        <div class="widget-content-white glossed">
            <div class="padded">
                <div class="pull-right" style="margin: 20px 0">
                    <a href="#" data-toggle="modal" data-target="#add_film" class="btn pull-right btn-default"
                       style="text-decoration: none"><i class="icon-plus"></i>Add Film</a>
                </div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Title</th>
                        <th>Actor</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td><input type="text" class="form-control" placeholder="Search" ng-model="searchName"
                                   name="table_search" style="padding: 2px">
                        </td>
                        <td><input type="text" class="form-control" placeholder="Search" ng-model="searchActor"
                                   name="table_search" style="padding: 2px">
                        </td>
                        <td>
                            <select class="form-control" ng-model="filterCategory"
                                    ng-options="item.Category.cat_name for item in categories"
                                    style="padding: 0"></select>
                        </td>
                        <td>

                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr ng-repeat="value in data | filter :filter_category| filter:searchName | filter:searchActor">
                        <td>{{$index+1}}</td>
                        <td>{{value.Film.film_title}}</td>
                        <td>{{value.Film.actor}}</td>
                        <td>{{value.Category.cat_name}}</td>
                        <td>
                            <span class="label label-success" ng-show="value.Film.film_status=='publish'">Publish</span>
                            <span class="label label-success" ng-show="value.Film.film_status=='trailer'">Trailer</span>
                            <span class="label label-danger" ng-show="value.Film.film_status=='unpublish'">Unpublish</span>
                            <span class="label label-danger" ng-show="value.Film.film_status=='draft'">Draft</span></td>
                        <td class="text-center">
                            <a href="#" ng-click="edit(value.Film.id)" data-toggle="modal" data-target="#edit_modal" class="btn btn-default btn-xs"><i class="icon-pencil"></i></a>
                            <a href="#"  ng-click =delete(value.Film.id,$index) class="btn btn-danger btn-xs"><i class="icon-remove"></i></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <pagination style="background-color: #F5F5F5" total-items="totalItems" ng-model="currentPage" ng-change="pageChanged(currentPage)" class="pagination-sm pull-right" items-per-page="perPage"></pagination>

        </div>
        <!-- Modal create -->
        <div id="add_film" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 800px">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Add Film</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <form name="form_create" role="form" method="POST" ng-submit="addFilm()">
                                <input type="hidden" ng-model="author"  ng-init="author=<?php echo AuthComponent::user('id')?>">
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <label for="thumbnail">Thumbnails</label>
                                        <div class="form-group text-center">
                                            <?php echo $this->Html->image('default_img/film_thumb_default.png', array('class' => 'img-responsive')) ?>
                                            <input type="file" id="thumbnail" ng-model='form.add.film_thumb'>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="name">Film Title</label>
                                            <input type="text" id="name" ng-model="form.add.film_title"
                                                   class="form-control" name="name"
                                                   placeholder="Film title">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="link_film">Link Film</label>
                                            <input type="url" id="link_film" class="form-control"
                                                   placeholder="Link Film"
                                                   name="link_film" ng-model="form.add.link_film">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="link_trailer">Link Trailer</label>
                                            <input type="text" class="form-control" placeholder="Link Trailer"
                                                   name="link_trailer" ng-model="form.add.link_trailer">
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <input type="text" class="form-control" placeholder="Quốc gia"
                                                   name="country" id="country" ng-model="form.add.country">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <select class="form-control" id="category" ng-model="category_selected"
                                                    ng-options="item.Category.cat_name for item in categories"
                                                    style="padding: 0"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="time_view">Time</label>
                                            <input type="text" class="form-control" placeholder="Thời lượng"
                                                   name="time" id="time_view" ng-model="form.add.time_view">
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="publishing_year">Publishing year</label>
                                            <input type="text" class="form-control" placeholder="Năm phát hành"
                                                   name="publishing_year" ng-model="form.add.publishing_year">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="director">Director</label>
                                            <input type="text" class="form-control" name="director"
                                                   placeholder="Đạo diễn"
                                                   ng-model="form.add.director"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="actor">Actor</label>
                                            <input type="text" class="form-control" placeholder="Diễn viên"
                                                   name="actor" id="actor" ng-model="form.add.actor">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="film_parent">Film parent</label>
                                            <select class="form-control" id="film_parent" ng-model="parent_selected"
                                                    ng-options="item.Film.film_title for item in list_parent"
                                                    style="padding: 0"></select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="satus">Status</label>
                                            <select class="form-control" id="status" ng-model="status_selected"
                                                    ng-options="item.status for item in statuses"
                                                    style="padding: 0"></select>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label for="film_content">Nội dung phim</label>
                                        <textarea name="film_content" class="form-control" id="film_content" cols="90"
                                                  rows="10" ng-model="form.add.film_content"></textarea>

                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit" name="submit">Save</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>


        <!--Modal edit-->
        <div id="edit_modal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 800px">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">EditFilm</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <form name="form_create" role="form" method="POST" ng-submit="saveEdit()">
                                <input type="hidden" ng-model="modifier"  ng-init="modifier=<?php echo AuthComponent::user('id')?>">
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <label for="thumbnail">Thumbnails</label>
                                        <div class="form-group text-center">
                                            <?php echo $this->Html->image('default_img/film_thumb_default.png', array('class' => 'img-responsive')) ?>
                                            <input type="file" id="thumbnail" ng-model='form.Film.film_thumb'>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="name">Film Title</label>
                                            <input type="text" id="name" ng-model="form.Film.film_title"
                                                   class="form-control" name="name"
                                                   placeholder="Film title">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="link_film">Link Film</label>
                                            <input type="url" id="link_film" class="form-control"
                                                   placeholder="Link Film"
                                                   name="link_film" ng-model="form.Film.link_film">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="link_trailer">Link Trailer</label>
                                            <input type="text" class="form-control" placeholder="Link Trailer"
                                                   name="link_trailer" ng-model="form.Film.link_trailer">
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <input type="text" class="form-control" placeholder="Quốc gia"
                                                   name="country" id="country" ng-model="form.Film.country">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <select class="form-control" id="category" ng-model="category_selected_edit"
                                                    ng-options="item.Category.cat_name for item in categories"
                                                    style="padding: 0"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="time_view">Time</label>
                                            <input type="text" class="form-control" placeholder="Thời lượng"
                                                   name="time" id="time_view" ng-model="form.Film.times_view">
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="publishing_year">Publishing year</label>
                                            <input type="text" class="form-control" placeholder="Năm phát hành"
                                                   name="publishing_year" ng-model="form.Film.publishing_year">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="director">Director</label>
                                            <input type="text" class="form-control" name="director"
                                                   placeholder="Đạo diễn"
                                                   ng-model="form.Film.director"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="actor">Actor</label>
                                            <input type="text" class="form-control" placeholder="Diễn viên"
                                                   name="actor" id="actor" ng-model="form.Film.actor">
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="film_parent">Film parent</label>
                                            <select class="form-control" id="film_parent" ng-model="parent_selected_edit"
                                                    ng-options="item.Film.film_title for item in list_parent"
                                                    style="padding: 0"></select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="satus">Status</label>
                                            <select class="form-control" id="status" ng-model="status_selected_edit"
                                                    ng-options="item.status for item in statuses"
                                                    style="padding: 0"></select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label for="film_content">Nội dung phim</label>
                                        <textarea name="film_content" class="form-control" id="film_content" cols="90"
                                                  rows="10" ng-model="form.Film.film_content"></textarea>

                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit" name="submit">Save</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php echo $this->start('javascript'); ?>
<script type="text/javascript">
    var films_json = '<?php echo json_encode($films)?>';
    var categories_json = '<?php echo json_encode($categories);?>';
</script>
<?php
echo $this->Html->script('app/controllers/film/manage_film');
echo $this->Html->script('app/services/myServices');
echo $this->end('javascript');
?>