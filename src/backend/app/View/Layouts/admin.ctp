<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    echo $this->Html->css(array(
        'bootstrap',
        'fullcalendar',
        'datatables/datatables',
        'datatables/bootstrap.datatables',
        'chosen',
        'font-awesome/font-awesome',
        'app'
    ));
    echo $this->Html->meta('icon');
    ?>
    <link href='http://fonts.googleapis.com/css?family=Oswald:300,400,700|Open+Sans:400,700,300' rel='stylesheet'
          type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    @javascript html5shiv respond.min
    <![endif]-->
    <?php
    echo $this->fetch('css');
    ?>
    <title>HK02 - Online Movie</title>
</head>

<body class="body-light-linen">

<div class="all-wrapper">
    <div class="row">

        <?php echo $this->element('menu'); ?>
        <div class="col-md-9">
            <div class="content-wrapper wood-wrapper wood-wrapper">
                <?php echo $this->element('header'); ?>
                <div class="main-content">
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->script([
    'jquery',
    'jquery.sparkline.min',
    'bootstrap/tab',
    'bootstrap/collapse',
    'bootstrap/transition',
    'bootstrap/tooltip',
    'jquery.knob',
    'chosen.jquery.min',
    'raphael-min',
    'application',
    'app/helper/myHelper'
]);
?>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
<?php
echo $this->fetch('javascript');
?>
</body>
</html>
