<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    echo $this->Html->css(['fullcalendar',
        'chosen', 'font-awesome/font-awesome', 'app']);
    ?>

    <link href='http://fonts.googleapis.com/css?family=Oswald:300,400,700|Open+Sans:400,700,300' rel='stylesheet'
          type='text/css'>
    <?php echo $this->Html->meta('icon'); ?>
    <link href="assets/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    @javascript html5shiv respond.min
    <![endif]-->

    <title>Login | HK02 - Online Movie</title>

</head>

<body class="body-light-linen">
<?php
echo $this->fetch('content');
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<?php
echo $this->Html->script([
    'jquery.sparkline.min',
    'bootstrap/tab',
    'bootstrap/collapse',
    'bootstrap/transition',
    'bootstrap/tooltip',
    'jquery.knob',
    'chosen.jquery.min',
    'raphael-min',
    'application',
]);
?>
</body>
</html>
