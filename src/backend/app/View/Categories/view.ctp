<div class="main-content" ng-app="category">
    <div class="widget" ng-controller="view">
        <div align="center">
            <h2>{{Category.cat_name}}</h2>
            <p>{{Category.cat_des}}</p>
        </div>
        <div>
            <h3 class="section-title first-title"><i class="icon-table"></i>Film Table
            </h3>
            <div class="widget-content-white glossed">
                <div class="padded">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Title</th>
                            <th>Actor</th>
                            <th>Category</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                            <td><input type="text" class="form-control" placeholder="Search" ng-model="searchName"
                                       name="table_search" style="padding: 2px">
                            </td>
                            <td><input type="text" class="form-control" placeholder="Search" ng-model="searchActor"
                                       name="table_search" style="padding: 2px">
                            </td>
                            <td>
                            </td>
                            <td>
                                <select class="form-control" ng-model="filterStatus"
                                        ng-options="item.status for item in statuses"
                                        style="padding: 0"></select>
                            </td>
                        </tr>
                        <tr ng-repeat="value in data | filter :filter_status| filter:searchName | filter:searchActor">
                            <td>{{$index+1}}</td>
                            <td>{{value.film_title}}</td>
                            <td>{{value.actor}}</td>
                            <td>{{Category.cat_name}}</td>
                            <td>
                                <span class="label label-success" ng-show="value.film_status=='publish'">Publish</span>
                                <span class="label label-success" ng-show="value.film_status=='trailer'">Trailer</span>
                                <span class="label label-danger" ng-show="value.film_status=='unpublish'">Unpublish</span>
                                <span class="label label-danger" ng-show="value.film_status=='draft'">Draft</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->start('javascript'); ?>
<script type="text/javascript">
    var data_json = '<?php echo json_encode($data)?>';
</script>
<?php
echo $this->Html->script('app/controllers/category/view');
echo $this->Html->script('app/services/myServices');
echo $this->end('javascript');
?>