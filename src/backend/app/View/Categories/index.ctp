<div class="main-content" ng-app="category">
    <div class="widget" ng-controller="manage_category">
        <h3 class="section-title first-title"><i class="icon-table"></i>Categories Table

        </h3>
        <div class="widget-content-white glossed">
            <div class="padded">
                <div class="pull-right" style=" margin: 20px 0">
                    <a href="#" data-toggle="modal" data-target="#add_category" class="btn btn-default"
                       style="text-decoration: none;"><i class="icon-plus"></i>Add Category</a>
                </div>
                <table class="table table-striped table-bordered table-hover datatable">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Film Number</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="value in data">
                        <td>{{$index+1}}</td>
                        <td><a href="Categories/view/{{value.Category.id}}" style="text-decoration: none">{{value.Category.cat_name}}</a></td>
                        <td>{{value.Category.cat_des}}</td>
                        <td >
                            <span ng-show="value.Film.length>0">{{value.Film.length}}</span>
                            <span ng-show="value.Film==null||value.Film.length==0">0</span>
                        </td>
                        <td class="text-center">
                            <a href="Categories/view/{{value.Category.id}}" class="btn btn-default btn-xs"><i class="icon-eye-open"></i></a>
                            <a href="#" class="btn btn-default btn-xs" ng-click="edit(value.Category.id)"  data-toggle="modal" data-target="#edit_category">
                                <i class="icon-pencil"></i></a>
                            <a href="#" ng-click="delete(value.Category.id,$index)" class="btn btn-danger btn-xs"><i
                                    class="icon-remove"></i></a>

                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <pagination style="background-color: #F5F5F5" total-items="totalItems" ng-model="currentPage" ng-change="pageChanged(currentPage)" class="pagination-sm pull-right" items-per-page="perPage"></pagination>

        </div>
        <!--Modal add-->
        <div id="add_category" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Category</h4>
                    </div>
                    <div class="modal-body">
                        <form >

                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="name_category">Name Category</label>
                                <input type="text" id="name_category" ng-model="form.add.Category.cat_name" name="name_category" placeholder="Name Category" class="form-control">
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="cat_des">Description</label>
                                <input type="text" id="cat_des" ng-model="form.add.Category.cat_des" name="des_category" placeholder="Name Category" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="#" ng-click="saveAdd()" class="btn btn-primary">Save</a>
                        <button type="button"  class="btn btn-default" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>


        <!--Modal-->
        <div id="edit_category" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Category</h4>
                    </div>
                    <div class="modal-body">
                        <form >
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="name_category">Name Category</label>
                                <input type="text" id="name_category" ng-model="form.Category.cat_name" name="name_category" placeholder="Name Category" class="form-control">
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="cat_des">Description</label>
                                <input type="text" id="cat_des" ng-model="form.Category.cat_des" name="des_category" placeholder="Name Category" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="#" ng-click="saveEdit()" class="btn btn-primary">Save</a>
                        <button type="button"  class="btn btn-default" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->start('javascript'); ?>
<!--<script type="text/javascript">
    var categories_json='<?php /*echo json_encode($categories);*/?>';
</script>-->
<?php
echo $this->Html->script('app/controllers/category/manage_category');
echo $this->Html->script('app/services/myServices');
echo $this->end('javascript');
?>
